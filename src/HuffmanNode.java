public class HuffmanNode implements Comparable<HuffmanNode> {
    public int chr;
    public int freq;
    public HuffmanNode left;
    public HuffmanNode right;

    public HuffmanNode() {
        freq = 0;
        chr = 0;
        left = null;
        right = null;
    }

    public HuffmanNode(int frq, HuffmanNode l, HuffmanNode r){
        freq = frq;
        left = l;
        right = r;
    }
    public HuffmanNode (int frq, int ch, HuffmanNode l, HuffmanNode r) {
        freq = frq;
        // Frequency needs to be sum of previous nodes
        chr = ch;
        left = l;
        right = r;
    }

    public String toString() {
        return "Freq: " + freq + " Char: " + chr;
    }

    @Override
    public int compareTo(HuffmanNode other) {
        // Flip order for alternate ordering
        return this.freq - other.freq;
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }
}
