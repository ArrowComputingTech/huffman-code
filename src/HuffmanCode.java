import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.sql.Array;
import java.util.*;

public class HuffmanCode {

    private String code;
    private HuffmanNode root = new HuffmanNode();

    public HuffmanCode(Scanner input) throws FileNotFoundException {
        boolean atAscii = true;
        ArrayList<String> asciiCodes = new ArrayList<>();
        ArrayList<String> bitCodes = new ArrayList<>();
        while (input.hasNextLine()) {
            if (atAscii) {
                asciiCodes.add(input.nextLine());
            } else {
                bitCodes.add(input.nextLine());
            }
            atAscii = !atAscii;
        }

        HuffmanNode currentNode = root;
        for (int i = 0; i < asciiCodes.size(); i++) {
            String[] bitString = bitCodes.get(i).split("");
            currentNode = root;
            for (String bit : bitString) {
                if (bit.equals("0")) {
                    if (currentNode.left != null) {
                        currentNode = currentNode.left;
                    } else {
                        HuffmanNode tempNode = new HuffmanNode();
                        currentNode.left = tempNode;
                        currentNode = tempNode;
                    }
                } else { // bit==1
                    if (currentNode.right != null) {
                        currentNode = currentNode.right;
                    } else {
                        HuffmanNode tempNode = new HuffmanNode();
                        currentNode.right = tempNode;
                        currentNode = tempNode;
                    }
                }
            }
            currentNode.chr = Integer.parseInt(asciiCodes.get(i));
        }
    }

    public HuffmanNode getRoot() {
        return this.root;
    }

    public HuffmanCode(int[] frequencies) {
        PriorityQueue<HuffmanNode> pq = new PriorityQueue<>(frequencies.length);
        for (int i = 0; i < frequencies.length; i += 1) {
            if (frequencies[i] > 0) {
                HuffmanNode hn = new HuffmanNode(frequencies[i], i, null, null);
                pq.add(hn);
            }
        }
        HuffmanNode rootTree = buildTree(pq);
        code = buildCode(rootTree);
    }

    private HuffmanNode buildTree(PriorityQueue<HuffmanNode> pq) {
        while (pq.size() > 1) {
            HuffmanNode hn00 = pq.poll();
            HuffmanNode hn01 = pq.poll();
            int frq = hn00.freq + hn01.freq;
            HuffmanNode parent = new HuffmanNode(frq, hn00, hn01);
            pq.add(parent);
        }
        return pq.poll();
    }

    private String buildCode(HuffmanNode rootTree) {
        return buildCodeKernel(rootTree, "");
    }

    private String buildCodeKernel(HuffmanNode node, String letterCode) {
        String legend = "";
        if (node.left == null && node.right == null) {
            return node.chr + "\n" + letterCode + "\n";
        } else {
            if (node.left != null) {
                legend += buildCodeKernel(node.left, letterCode + "0");
            }
            if (node.right != null) {
                legend += buildCodeKernel(node.right, letterCode + "1");
            }
        }
        return legend;
    }

    private String getCode() {
        return code;
    }

    public void save(PrintStream output) {
        String legend = this.getCode();
        output.print(legend);
    }

    public void translate(BitInputStream input, PrintStream output) {
        String outString = "";
        HuffmanNode currentNode = getRoot();
        String bit = "";
        while (input.hasNextBit()) {
            if (currentNode.isLeaf()) {
                outString += (Character.toString(currentNode.chr));
                currentNode = getRoot();
            } else {
                bit = String.valueOf(input.nextBit());
                currentNode = bit.equals("0") ? currentNode.left : currentNode.right;
            }
        }
        outString += (Character.toString(currentNode.chr));
        output.println(outString);
    }
}